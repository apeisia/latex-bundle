<?php
namespace Apeisia\LatexBundle\Twig;

use Symfony\Component\HttpKernel\Config\FileLocator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Filter extends AbstractExtension {

    public function getFilters() {
        return [
            new TwigFilter('nl2tep', array($this, 'nl2tep'), array('is_safe' => array('tex', 'html'))),
            new TwigFilter('nl2tnl', array($this, 'nl2tnl'), array('is_safe' => array('tex', 'html'))),
        ];
    }

    public function nl2tep($text) {
        return str_replace("\n", "~\\\\\n", $text);
    }

    public function nl2tnl($text) {
        return str_replace("\n", "~\\newline\n", $text);
    }

    public function getName() {
        return 'apeisia_latex_filter_extension';
    }
}
