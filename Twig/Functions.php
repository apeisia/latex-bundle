<?php

namespace Apeisia\LatexBundle\Twig;

use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Symfony\Component\Mime\MimeTypes;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use function GuzzleHttp\Promise\inspect;

class Functions extends AbstractExtension
{
    private $files = [];
    private $guesser;
    private $fileNum = 0;
    private $twig;
    private $env;

    public function __construct(\Twig\Environment $twig, string $env)
    {
        $this->twig = $twig;
        $this->env  = $env;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('includegraphics', [$this, 'includegraphics'], ['is_safe' => ['tex']]),
            new TwigFunction('texres', [$this, 'texres']),
            new TwigFunction('texfile', [$this, 'texfile']),
            new TwigFunction('texvfile', [$this, 'texvfile']),
            new TwigFunction('texdataurl', [$this, 'texdataurl']),
            new TwigFunction('texphpfile', [$this, 'texphpfile']),
        ];
    }

    public function includegraphics($file, $options = '')
    {
        if ($options == '' && array_key_exists($file, $this->files) && count($this->files[$file]) >= 3) {
            $size = $this->files[$file][2];
            if ($size['width'] > 500) {
                $options = 'width=\\textwidth';
            }
        }
        try {
            return '\\includegraphics' . ($options != '' ? '[' . $options . ']' : '') . '{' . $file . '}';
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    private function texResourceOrFile(string $name, string $type = 'resource') 
    {
        $tmpname   = md5($name);
        $e         = explode('.', basename($name));
        $l         = count($e);
        $isImagick = false;
        if ($l > 0) {
            if ($e[$l - 1] == 'twig' && $l > 1) {
                $tmpname .= '.' . $e[$l - 2];
            } else {
                $tmpname .= '.' . $e[$l - 1];
                if ($this->isImagickFile($e[$l - 1])) {
                    $isImagick = true;
                    $file      = $this->twig->getLoader()->getSourceContext($name)->getPath();
                    $tmpname   = $this->imagickFile(new \Imagick($file), $e[$l - 1], $tmpname);
                }
            }
        }
        if (!$isImagick) {
            $this->files[$tmpname] = [$type, $name];
        }

        return $tmpname;
    }

    /**
     * Add the given resource location (i.e. @App/something/something.png) to the pdf. For absolute paths use texfile() instead
     */
    public function texres($resource)
    {
        return $this->texResourceOrFile($resource, 'resource');
    }

    /**
     * Add the given file to the pdf
     */
    public function texfile($path)
    {
        return $this->texResourceOrFile($path, 'file');
    }

    public function texphpfile(?\SplFileInfo $file, bool $useGuessedExtension = false, ?string $forceExtension = null)
    {
        // disable php deprecation errors (some library triggers a php 8.2 deprecation)
        \error_reporting(\E_ALL ^ \E_DEPRECATED);
        
        $error = false;

        if ($file === null) {
            $error = true;
        } else {
            try {
                $ext = $forceExtension ?? ($useGuessedExtension && $file instanceof File ? $file->guessExtension() : $file->getExtension());
                if ($this->isImagickFile($ext)) {
                    $tmpname = $this->imagickFile(new \Imagick($file->getPathname()), $ext);
                } else {
                    $tmpname               = md5($file->getPathname()) . '.' . $ext;
                    $this->files[$tmpname] = ['file', $file->getPathname()];
                }
            } catch (\Exception $e) {
                $error = true;
            }
        }

        if ($error) {
            $file = 'empty';
            if ($this->env == 'dev') {
                $file = 'noimg';
            }
            $tmpname = $this->imagickFile(new \Imagick(__DIR__ . '/../Resources/images/' . $file . '.png'), 'png');
        }
        
        return $tmpname;
    }

    /**
     * Add the given file content as a virtual file
     */
    public function texvfile($content, $fileExtension, $trim = false)
    {
        if ($this->isImagickFile($fileExtension)) {
            $im = new \Imagick();
            $im->readImageBlob($content);

            if ($trim) {
                $im->trimImage(0);
            }

            $tmpname = $this->imagickFile($im, $fileExtension);
        } else {
            $tmpname               = md5($content) . '.' . $fileExtension;
            $this->files[$tmpname] = ['content', $content];
        }

        return $tmpname;
    }

    private function isImagickFile($extension)
    {
        return in_array(strtolower($extension), ['png', 'jpg', 'jpeg', 'gif']);
    }

    private function imagickFile(\Imagick $im, $originalExtension, $tmpname = null)
    {
        $ext     = [
            'png'  => 'png',
            'gif'  => 'png',
            'jpg'  => 'jpg',
            'jpeg' => 'jpg',
        ];
        $destExt = $ext[strtolower($originalExtension)];
        if (!$tmpname) {
            $tmpname = md5($this->fileNum++) . '.' . $destExt;
        }
        if (array_key_exists($tmpname, $this->files)) {
            return $tmpname;
        }
        $size = $im->getImageGeometry();

        $new = new \Imagick();
        $new->newImage($size['width'], $size['height'], new \ImagickPixel('white'), $destExt);
        if ($destExt == 'jpg') {
            $new->setCompressionQuality(95);
        }
        $new->compositeImage($im, \Imagick::COMPOSITE_OVER, 0, 0);

        $this->files[$tmpname] = ['content', $new->getImageBlob(), $size];

        $im->destroy();
        $new->destroy();

        return $tmpname;
    }

    public function texdataurl($dataUrl, $trim = false)
    {
        if (substr($dataUrl, 0, 5) != 'data:') {
            throw new \Exception('texvdataurl: expected data url with "data:", got "' . substr($dataUrl, 0, 5) . '"');
        }
        $e = explode(',', $dataUrl);
        if (count($e) != 2) {
            throw new \Exception('texvdataurl: malformated url. Expected exactly one ",", found ' . (count($e) - 1) . '.');
        }

        preg_match('#^data:(\w+/\w+);base64$#', $e[0], $matches);
        $mime = $matches[1];
        if (!$this->guesser) {
            $this->guesser = new MimeTypes();
        }
        $ext = $this->guesser->getExtensions($mime);
        if (count($ext) === 0) {
            throw new \Exception('texvdataurl: could not guess file extension for mime type  ' . $mime . '.');
        }

        return $this->texvfile(base64_decode($e[1]), $ext[0], $trim);
    }

    public function getName()
    {
        return 'apeisia_latex_texres_extension';
    }

    public function reset()
    {
        $this->files = [];
    }

    public function getFiles()
    {
        return $this->files;
    }

}
