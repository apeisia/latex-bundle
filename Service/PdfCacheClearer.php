<?php
namespace Apeisia\LatexBundle\Service;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;
use Symfony\Component\Filesystem\Filesystem;


class PdfCacheClearer implements CacheClearerInterface
{

    public function clear($cacheDir)
    {
        $fs = new Filesystem();
        $fs->remove($cacheDir.'/pdf');
    }
}
