<?php

namespace Apeisia\LatexBundle\Generator;

use Apeisia\BaseBundle\Service\ContentDispositionHelper;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class LaTex
{
    private LaTexGenerator $generator;

    public function __construct(LaTexGenerator $generator)
    {
        set_time_limit(0);
        $this->generator = $generator;
    }

    /**
     * @param string $resource
     * @param array $twigVariables
     * @return string
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function pdfZip(string $resource, $twigVariables = []): string
    {
        return $this->generator->generate($resource, $twigVariables, true);
    }

    /**
     * @param string $resource
     * @param string $outputFile
     * @param array $twigVariables
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function pdfZipFile(string $resource, string $outputFile, $twigVariables = [])
    {
        file_put_contents($outputFile, $this->pdfZip($resource, $twigVariables));
    }

    /**
     * @param string $resource
     * @param array $twigVariables
     * @return string
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function pdfString(string $resource, $twigVariables = []): string
    {
        return $this->generator->generate($resource, $twigVariables);
    }

    /**
     * @param string $resource
     * @param string $outputFile
     * @param array $twigVariables
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function pdfFile(string $resource, string $outputFile, $twigVariables = [])
    {
        file_put_contents($outputFile, $this->pdfString($resource, $twigVariables));
    }

    public function pdfTempFile(string $resource, $twigVariables = []): \SplFileInfo
    {
        $string = $this->pdfString($resource, $twigVariables);
        $tempFile = tempnam(sys_get_temp_dir(), 'pdf');
        file_put_contents($tempFile, $string);
        
        return new \SplFileInfo($tempFile);
    }

    /**
     * @param string $resource
     * @param array $twigVariables
     * @return Response
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function pdfResponse(
        string $resource,
        array  $twigVariables = [],
               $disposition = HeaderUtils::DISPOSITION_INLINE,
               $filename = null
    ): Response {
        if ($filename) {
            $disposition = ContentDispositionHelper::makeDisposition($filename, $disposition);
        }
        return new Response($this->pdfString($resource, $twigVariables), 200, [
            'Content-Type'        => 'application/pdf',
            'Cache-Control'       => 'no-cache, no-store, must-revalidate',
            'Content-Disposition' => $disposition,
        ]);
    }

}
