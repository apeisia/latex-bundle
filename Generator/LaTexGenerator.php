<?php
namespace Apeisia\LatexBundle\Generator;

use Apeisia\LatexBundle\Twig\Functions;
use LogicException;
use PharData;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Extension\CoreExtension;
use Twig\Extension\EscaperExtension;

class LaTexGenerator {
    private Environment $twig;
    private Functions $texRes;
    private string $cacheDir;

    public function __construct(Environment $twig, Functions $texRes, string $cacheDir) {
        $this->twig = new Environment(
            $twig->getLoader(),
            array(
                'cache' => $twig->getCache(),
                'auto_reload' => true,
                'autoescape' => 'tex',
                'debug' => $twig->isDebug(),
                'strict_variables' => $twig->isStrictVariables(),
            )
        );
        foreach($twig->getExtensions() as $ext) {
            if(!$this->twig->hasExtension(get_class($ext))) {
                $this->twig->addExtension($ext);
            }
        }
        /** @var EscaperExtension $core */
        $core = $this->twig->getExtension(EscaperExtension::class);
        $core->setEscaper('tex', function($twig, $string) {
            return Escaper::escape($string);
        });
        $this->texRes = $texRes;
        $this->cacheDir = $cacheDir;
    }

    /**
     * @param string $file
     * @param array $twigVariables
     * @param bool $returnZip
     * @return string
     * @throws LaTexException
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function generate(string $file, $twigVariables = [], $returnZip = false) {
        $fs = new Filesystem();
        // create a temp file, delete it and then create a directory with that name
        $tempdir = tempnam(sys_get_temp_dir(), 'ApeisiaLaTeX');
        $fs->remove($tempdir);
        $fs->mkdir($tempdir);
        $tmpname = basename($file);
        if(substr($tmpname, -5) == '.twig') $tmpname = substr($tmpname, 0, -5);
        $cacheKeySource = $this->populate($tempdir, [$tmpname => ['resource', $file]], $twigVariables);
        $cacheKey = sha1($cacheKeySource);
        $cacheDir = $this->cacheDir.'/pdf/'.substr($cacheKey, 0, 2);
        $cacheFile = $cacheDir.'/'.$cacheKey.'.pdf';

        if(file_exists($cacheFile) && !$returnZip && !$this->twig->isDebug()) {
            if(!getenv('TEX_KEEP_TEMP')) $fs->remove($tempdir);
            return file_get_contents($cacheFile);
        }
        $curdir = getcwd();
        chdir($tempdir);
        try {
            $out = $this->compile($tmpname);
            if($returnZip) {
                $zipfile = tempnam(sys_get_temp_dir(), 'ApeisiaLaTeX');
                $fs->remove($zipfile);
                $zipfile .= '.zip';
                $p = new PharData($zipfile);
                $p->buildFromDirectory($tempdir);
                unset($p);
                $out = file_get_contents($zipfile);
                $fs->remove($zipfile);
            } else {
                if(!is_dir($cacheDir))
                    mkdir($cacheDir, 0770, true);
                file_put_contents($cacheFile, $out);
            }
        } finally {
            chdir($curdir);
            if(!getenv('TEX_KEEP_TEMP')) $fs->remove($tempdir);
        }
        return $out;
    }

    /**
     * @param $tempdir
     * @param $files
     * @param $twigVariables
     * @return string
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function populate($tempdir, $files, $twigVariables) {
        $cacheKeySource = '';
        foreach($files as $tmpname => $f) {
            $out = $tempdir.'/'.$tmpname;
            if($f[0] == 'resource') {
                $file = $f[1];
                $cfile = $this->twig->getLoader()->getSourceContext($file)->getPath();
                if (substr($file, -5) == '.twig') {
                    $this->texRes->reset();
//                    $tpl = $this->twig->createTemplate('{% autoescape "tex" %}'.file_get_contents($cfile).'{% endautoescape %}');
                    $tpl = $this->twig->createTemplate(file_get_contents($cfile));
                    $content = $tpl->render($twigVariables);
                    // i really don't know why this happens sometimes...
                    $content = strtr($content, [
                        '&quot;' => '"',
                        '&amp;' => '\&',
                    ]);
                    file_put_contents($out, $content);
                    $cacheKeySource .= $content;
                    $cacheKeySource .= $this->populate($tempdir, $this->texRes->getFiles(), $twigVariables);
                } else {
                    copy($cfile, $out);
                    $cacheKeySource .= filemtime($cfile);
                }
            } else if($f[0] == 'file') {
                copy($f[1], $out);
                $cacheKeySource .= filemtime($f[1]);
            } else if($f[0] == 'content') {
                $content = $f[1];
                file_put_contents($out, $content);
                $cacheKeySource .= $content;
            } else {
                throw new LogicException('unexpected type ' . $f[0]);
            }
        }
        return $cacheKeySource;
    }

    /**
     * @param string $mainfile
     * @return bool|string
     * @throws LaTexException
     */
    private function compile(string $mainfile) {
        $cmd = [
            dirname(dirname(__DIR__)) . '/tectonic-bin/tectonic',
            '--print',
            $mainfile,
        ];

        $filesBefore = glob('*.pdf');
        $process = new Process($cmd, null, [
            'TECTONIC_BUNDLE' => $_ENV['TECTONIC_BUNDLE'] ?? null,
        ], null, 600);
        $process->mustRun();

        $files = array_diff(glob('*.pdf'), $filesBefore);

        if(!$files || count($files) == 0) {
            $log = $this->getLog();
            if(!$log) {
                throw new LaTexException($process->getOutput().$process->getErrorOutput());
            } else {
                throw new LaTexException($log);
            }
        }

        return file_get_contents(array_values($files)[0]);
    }

    private function getLog() {
        $log = glob('*.log');
        if(count($log) >= 1) {
            return file_get_contents($log[0]);
        }
        return '';
    }
}
