<?php
namespace Apeisia\LatexBundle\Generator;

class Escaper {

    public static function escape($string) {
        $string = str_replace(
            array(
                '\\',
                '{',
                '}',
                '\\{\\textbackslash\\}',
                '$',
                '_',
                '&',
                '#',
                '%',
                '"',
                //'ß',
                "'",
            ),
            array(
                '{\\textbackslash}',
                '\\{',
                '\\}',
                '{\\textbackslash}',
                '\\$',
                '\\_',
                '\\&',
                '\\#',
                '\\%',
                '\\grqq{}',
                //'{\\ss}',
                '\\grq{}',
            ),
            $string
        );

        return $string;
    }
}
