# LaTex Bundle
Template:

    \documentclass[12pt]{article}
    \usepackage{graphicx}
    \begin{document}
    
    Hallo Welt
    
    texres() nimmt ein Resource Pfad (@App...) und sorgt dafür, dass
    pdflatex ein richtigen Dateinamen bekommt. Twig Dateien werden
    Ausgeführt.
    
    Volle version mit blödem Klammerproblem:
    \includegraphics{{ ('{' ~ texres('pdf/test.jpg') }}}
    
    Einfacher:
    {{ includegraphics('pdf/test.jpg') }}
    
    {% for i in 1..100 %}
        {{ i }}
    {% endfor %}
    \end{document}


Generieren:

        $latex = $container->get(\Apeisia\LatexBundle\Generator\LaTex::class);
        // als Datei
        $latex->pdfFile('@App...test.tex.twig', 'out.pdf', ['var' => 42]);
        // als http Response
        $latex->pdfResponse('@App...test.tex.twig', ['var' => 42]);
        // oder string
        $latex->pdfString('@App...test.tex.twig', ['var' => 42]);
        
Komische Fehler und irgendwie gehts nicht weiter?

-> Working-Dir inkl Logs von pdflatex als zip

        $latex->pdfZipFile('@App...test.tex.twig', 'out.zip', ['var' => 42]);
            
