<?php
namespace Apeisia\LatexBundle\Tests;

use Apeisia\LatexBundle\Generator\Escaper;
use PHPUnit\Framework\TestCase;

if(!class_exists(Escaper::class)) {
    require_once __DIR__.'/../Generator/Escaper.php';
}

class EscaperTest extends TestCase
{
    public function testEsaping()
    {
        $this->assertEquals(
            '{\\textbackslash} \\$ \\&',
            Escaper::escape('\\ $ &')
        );
        $this->assertEquals(
            '{\\textbackslash}\\&',
            Escaper::escape('\\&')
        );
    }
}
